#!/bin/bash

docker run    -v ${PWD}/notebooks:/notebooks   registry.gitlab.com/brie-professional-development/custom-image-for-speed   jupyter nbconvert  /notebooks/*.ipynb
